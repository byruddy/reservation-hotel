-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 01 Mei 2018 pada 08.25
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_hotelv1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoice`
--

CREATE TABLE `invoice` (
  `id` int(20) NOT NULL,
  `room_number` int(5) NOT NULL,
  `room_type` int(3) NOT NULL,
  `check_in` date NOT NULL,
  `night` int(2) NOT NULL,
  `guest_name` varchar(50) NOT NULL,
  `typeid` varchar(15) NOT NULL,
  `numberid` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `cost` varchar(10) NOT NULL,
  `code_booked` char(6) NOT NULL,
  `admin_id` int(5) NOT NULL,
  `date_order` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `status` enum('waiting','success','lock') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `invoice`
--

INSERT INTO `invoice` (`id`, `room_number`, `room_type`, `check_in`, `night`, `guest_name`, `typeid`, `numberid`, `phone`, `email`, `address`, `cost`, `code_booked`, `admin_id`, `date_order`, `date_update`, `status`) VALUES
(1, 1, 2, '2017-12-20', 2, 'Rudi Hikmatullah', 'KTP', '082347392849238', '081238399388', 'rudihikmatullah@gmail.com', 'Bojonegoro, Serang - Indonesia, 23029', '782,000', 'BIYFKT', 1, '2018-01-01 11:07:02', '2018-01-03 19:25:48', 'lock'),
(2, 0, 2, '2018-01-01', 1, 'Nandiya Putri', 'passport', '929189128982198', '082713991281', 'nandiyaputri@ymail.com', 'BBS II, Jl Kemuning, Cilegon - Banten, 29173', '391,344', '2S9X8R', 0, '2018-01-01 11:37:13', '0000-00-00 00:00:00', 'waiting'),
(3, 2, 1, '2018-01-03', 7, 'Ricky Hidayatullah', 'ktp', '209024932842304', '081728930230', 'ricky@gmail.com', 'Ketileng, Cilegon - Banten, 48012', '3,528,893', 'AXT1FL', 0, '2018-01-01 11:42:58', '2018-01-01 13:11:15', 'success'),
(4, 1, 1, '2018-01-01', 1, 'Hudan Radifan Q', 'SIM', '203909203940929', '081273903999', 'hudan@gmail.com', 'Warnasari, Kec Ciwandan, Cilegon - Banten, 48102', '504,000', '1PCLBI', 1, '2018-01-01 13:10:51', '2018-01-03 18:25:49', 'lock'),
(5, 3, 1, '2018-01-03', 1, 'Fera Fajriayana', 'lainnya', '294230428200002', '081273940303', 'ferafajriayana@gmail.com', 'Citra Garden BMW, Serang, 30291', '504,286', 'HJDL1H', 0, '2018-01-01 13:14:15', '2018-01-03 18:25:31', 'lock'),
(6, 1, 2, '2018-01-05', 2, 'Alexander Abraham', 'passport', '291020003829999', '081263899282', 'alexander@gmail.com', 'New York, 29103', '782,262', 'Q0NQOA', 0, '2018-01-05 12:42:34', '2018-03-17 04:10:47', 'success'),
(7, 1, 1, '2018-01-05', 2, 'Wendi Nugraha', 'passport', '203820001283000', '081278340000', 'wendi@gmail.com', 'Jakarta, 30293', '1,008,184', 'M8VKCV', 0, '2018-01-05 14:49:08', '2018-01-05 14:53:40', 'lock'),
(8, 1, 1, '2018-01-05', 2, 'jarot', 'ktp', '17736891876', '08214253746', 'jarot@gmail.com', 'banten, 29838', '1,008,391', 'V1SZIB', 0, '2018-01-05 14:56:28', '2018-01-05 15:00:25', 'lock'),
(9, 1, 1, '2018-01-05', 2, 'jarot', 'ktp', '1526576286829', '08241437658', 'jarot@gmail.com', 'jakarta, 872861', '1,008,318', 'X3WLGA', 0, '2018-01-05 15:54:32', '2018-01-05 15:57:21', 'lock'),
(10, 0, 1, '2018-03-17', 1, 'Deni', 'ktp', '389324923919218', '081273823832', 'deni@gmail.com', 'Cileogn, 32322', '504,227', 'T1D4JP', 0, '2018-03-17 08:22:22', '0000-00-00 00:00:00', 'waiting');

-- --------------------------------------------------------

--
-- Struktur dari tabel `payment`
--

CREATE TABLE `payment` (
  `id` int(5) NOT NULL,
  `code_booked` char(6) NOT NULL,
  `jml_pembayaran` varchar(10) NOT NULL,
  `an_pengirim` varchar(50) NOT NULL,
  `bukti_trf` varchar(150) NOT NULL,
  `tgl` datetime NOT NULL,
  `status` enum('waiting','success','reject') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `payment`
--

INSERT INTO `payment` (`id`, `code_booked`, `jml_pembayaran`, `an_pengirim`, `bukti_trf`, `tgl`, `status`) VALUES
(1, 'BIYFKT', '35,443,543', 'Gugun Gunawan', '3.jpg', '2018-01-03 20:09:35', 'reject'),
(2, '2S9X8R', '30,000,000', 'Dina', '32.jpg', '2018-01-03 21:02:06', 'reject'),
(3, 'M8VKCV', '12,000,000', 'Hudan', '1.JPG', '2018-01-05 14:51:37', 'reject'),
(4, 'V1SZIB', '12,000,000', 'jarot', '11.JPG', '2018-01-05 14:58:14', 'success'),
(5, 'Q0NQOA', '23,920,482', 'Fanny', '52.jpg', '2018-01-05 15:02:31', 'reject'),
(6, 'X3WLGA', '1,000,000', 'jarot', '12.JPG', '2018-01-05 15:55:07', 'success'),
(7, '2S9X8R', '32,131,291', 'Yeni', '3.jpg', '2018-03-17 04:14:30', 'reject');

-- --------------------------------------------------------

--
-- Struktur dari tabel `room`
--

CREATE TABLE `room` (
  `id` int(5) NOT NULL,
  `number_room` int(10) NOT NULL,
  `type_room` int(3) NOT NULL,
  `status` enum('ready','use','maintenance') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `room`
--

INSERT INTO `room` (`id`, `number_room`, `type_room`, `status`) VALUES
(1, 1, 1, 'ready'),
(2, 2, 1, 'use'),
(3, 3, 1, 'ready'),
(4, 1, 2, 'use'),
(5, 2, 2, 'ready'),
(6, 3, 2, 'ready'),
(7, 4, 2, 'ready'),
(8, 1, 3, 'ready'),
(9, 2, 3, 'ready'),
(10, 3, 3, 'ready'),
(11, 4, 3, 'ready'),
(12, 5, 3, 'ready');

-- --------------------------------------------------------

--
-- Struktur dari tabel `room_type`
--

CREATE TABLE `room_type` (
  `id` int(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` varchar(10) NOT NULL,
  `status` enum('active','nonactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `room_type`
--

INSERT INTO `room_type` (`id`, `name`, `price`, `status`) VALUES
(1, 'Deluxe', '504000', 'active'),
(2, 'Superior', '391000', 'active'),
(3, 'Standard', '237000', 'active');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `username` char(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `photo` text NOT NULL,
  `status` enum('active','nonactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `photo`, `status`) VALUES
(1, 'admin', '15180959301e8d710af64b27ff2fd22c9f37e82c', 'Administrator', '', 'active'),
(2, 'ruddy', 'e15ff69eb79fce040a2068da02b3c4eedc4ed584', 'Rudi Hikmatullah', '', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_id` (`room_number`),
  ADD KEY `staff_id` (`admin_id`),
  ADD KEY `code_booked` (`code_booked`),
  ADD KEY `room_type` (`room_type`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_room` (`type_room`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
